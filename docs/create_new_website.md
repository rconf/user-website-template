## How to set up a new useR! website

These instructions are for setting up a new useR! website based on the Hugo template at https://gitlab.com/rconf/user-website-template.

This will be done by a member of the R Foundation Conference Committee ready to pass on to the useR! organizing team.

### Create a new repository

1. Log on to GitLab with the R-conferences account and navigate to the 
R Foundation Conferences group: https://gitlab.com/rconf.

2. Create a new project with a blank repository for the year the site will be for:
 - Project name: useR [YEAR] website.
 - Project URL (follows from above): https://gitlab.com/rconf/user-[YEAR]-website.git.
 - Visibility: public.
 - Do not set a project deployment target. 
 - Do not initialize with a README.
 - Do not enable Static Application Security Testing (SAST).

### Set the repo as a mirror of the main repository

1. In a terminal with access to git, navigate to a directory where you will clone the repository.

2. Create a bare clone of the repository.

    ```
    git clone --bare https://gitlab.com/rconf/user-website-template
    ```

3. Mirror-push to the new repository.
    
    ```
    cd user-website-template.git
    git push --mirror https://gitlab.com/rconf/user-[YEAR]-website.git
    ```
    Note, if you get the error 
    
    ```
    remote: HTTP Basic: Access denied
    ```
    
    you may need to run the following command first to be prompted 
    for your username and password:
    
    ```
    git config --system --unset credential.helper
    ```
    
    (if using the Windows command prompt you will need to be running as Administrator).
    Alternatively, you can include your crentials in the call to `git push`

    ```
    git push --mirror https://[username]:[password]@gitlab.com/rconf/user-[YEAR]-website.git
    ```

    or with a Personal Access Token (PAT)

    ```
    git push --mirror https://oauth2:glpat-[PAT]@gitlab.com/rconf/user-[YEAR]-website.git
    ```

4. Remove the temporary local repository you created in step 1.

    ```
    cd ..
    rm -rf user_hugo_template.git
    ```
    
### Deploy to Netlify

Create a new site as follows:

1. Login to Netlify via the R-conferences GitLab account.
2. On the Sites tab click "Add new site" > "Import an existing project"
3. Select GitLab as the git provider.
4. Select the repository created in the previous steps (select "R Foundation Conferences" as the account to find repos from)
5. Use the build command `hugo`.
6. Use the publish directory `public`.

Then set up branch deployment:

1. Select the new site on the Sites tab page. 
2. Go to Site settings > Build & deploy > Continuous Deployment > Branches, and select Edit settings. Select "Branch deploys: All".

### Set up R project URL

See instructions in private useRadmin repo: https://gitlab.com/rconf/useRadmin/-/blob/master/website/setting_up_new%20website.md.

